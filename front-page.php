<?php
/**
 * Template Name: Front Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<!-- <?php 
$args = array(
    'post_type'  => 'post',
    'meta_key' => '_thumbnail_id',
    'post_count' => 1 );
$query = new WP_Query($args);

$query->the_post();
if ( $query->have_posts() ) { 
    $title = get_the_title();
    $thumbnail = get_the_post_thumbnail_url( $post, 'large' );
    $excerpt = get_the_excerpt();
    $link = get_permalink();
    //$category = 
    $human_time = human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) );
?>
<div class="container container-fluid">
    <a href="<?php echo $link  ?>">
        <div class="" style="background:url(<?php echo $thumbnail ?>)">
            <div>
                <h1><?php echo $title ?></h1>
                <p><?php echo $excerpt ?></p>
                <span class="post-category"><?php echo $human_time." назад" ?></span>
            </div>
        </div>
    </a>
</div>
<?php } ?> -->

<div class="wrapper" id="full-width-page-wrapper">
    
    <?php   
            // $args = array(
            //     'numberposts' => 10,
            //     'offset' => 0,
            //     'category' => 0,
            //     'orderby' => 'post_date',
            //     'order' => 'DESC',
            //     'include' => '',
            //     'exclude' => '',
            //     'meta_key' => '',
            //     'meta_value' =>'',
            //     'post_type' => 'post',
            //     'post_status' => 'draft, publish, future, pending, private',
            //     'suppress_filters' => true
            // );
            
            // $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
            // if ( $recent_posts->have_posts() ) { // you never checked to see if no posts were found
            //     while($recent_posts->have_posts()) { 
            //         $recent_posts->the_post();
            //         $title = get_the_title();
            //         $thumbnail = get_the_post_thumbnail_url( $post, 'large' );
            //         $excerpt = get_the_excerpt();
            //         $link = get_permalink();
            //         //$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
            //         // Calculate aspect ratio: h / w * 100%.
            //     }
            // }
    ?>

    



    <div class="container border-top">
        <h2 class="text-center display-4 py-5 my-4">Новости</h2>
        <div class="row"> 
        <?php 
            
            $args = array(
                'post_type'  => 'post',
                'meta_key' => '_thumbnail_id',
                'post_count' => 6 );
            $query = new WP_Query($args);
            if ( $query->have_posts() ) { // you never checked to see if no posts were found
                while($query->have_posts()) { 
                    $query->the_post();
                    $title = get_the_title();
                    $thumbnail = get_the_post_thumbnail_url( $post, 'large' );
                    $excerpt = get_the_excerpt();
                    $link = get_permalink();
                    //$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
                    // Calculate aspect ratio: h / w * 100%.
                    ?>
                    <div class="col-md-6 col-sm-12">
                        <div class="card" >
                            <img class="card-img-top img-fluid" src="<?php echo $thumbnail ?>" alt="Card image cap">
                            <div class="card-body">
                                <h3 class="card-title mb-0">
                                    <a href="<?php echo $link ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h3>
                                <div class="mb-1 text-muted"><?php echo the_time('M j, Y')  ?></div>
                                <p class="card-text"><?php echo $excerpt ?></p>
                            </div>
                        </div>
                    </div> 
                <?php }
            } else {
                echo '<p>no posts found</p>';
            }   ?>
            </div>


    </div>


</div><!-- Wrapper end -->
    
<?php get_footer(); ?>
