<?php
function child_theme_setup() {
	// override parent theme's 'more' text for excerpts
	remove_filter( 'wp_trim_excerpt', 'understrap_all_excerpts_get_more_link' ); 
}
add_action( 'after_setup_theme', 'child_theme_setup' );





function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}


// First, make sure Jetpack doesn't concatenate all its CSS
// add_filter( 'jetpack_implode_frontend_css', '__return_false' );

// // Then, remove each CSS file, one at a time
// function jeherve_remove_all_jp_css() {
//   wp_deregister_style( 'AtD_style' ); // After the Deadline
//   wp_deregister_style( 'jetpack_likes' ); // Likes
//   wp_deregister_style( 'jetpack_related-posts' ); //Related Posts
//   wp_deregister_style( 'jetpack-carousel' ); // Carousel
//   wp_deregister_style( 'grunion.css' ); // Grunion contact form
//   wp_deregister_style( 'the-neverending-homepage' ); // Infinite Scroll
//   wp_deregister_style( 'infinity-twentyten' ); // Infinite Scroll - Twentyten Theme
//   wp_deregister_style( 'infinity-twentyeleven' ); // Infinite Scroll - Twentyeleven Theme
//   wp_deregister_style( 'infinity-twentytwelve' ); // Infinite Scroll - Twentytwelve Theme
//   wp_deregister_style( 'noticons' ); // Notes
//   wp_deregister_style( 'post-by-email' ); // Post by Email
//   wp_deregister_style( 'publicize' ); // Publicize
//   wp_deregister_style( 'sharedaddy' ); // Sharedaddy
//   wp_deregister_style( 'sharing' ); // Sharedaddy Sharing
//   wp_deregister_style( 'stats_reports_css' ); // Stats
//   wp_deregister_style( 'jetpack-widgets' ); // Widgets
//   wp_deregister_style( 'jetpack-slideshow' ); // Slideshows
//   wp_deregister_style( 'presentations' ); // Presentation shortcode
//   wp_deregister_style( 'jetpack-subscriptions' ); // Subscriptions
//   wp_deregister_style( 'tiled-gallery' ); // Tiled Galleries
//   wp_deregister_style( 'widget-conditions' ); // Widget Visibility
//   wp_deregister_style( 'jetpack_display_posts_widget' ); // Display Posts Widget
//   wp_deregister_style( 'gravatar-profile-widget' ); // Gravatar Widget
//   wp_deregister_style( 'widget-grid-and-list' ); // Top Posts widget
//   wp_deregister_style( 'jetpack-widgets' ); // Widgets
// }
// add_action('wp_print_styles', 'jeherve_remove_all_jp_css' );
// 
//add_image_size('featuredImageCropped', 250, 200, true);
//add_image_size('featuredImageBackground', 1250, 700, true);
function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.2.1.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
    global $post;
 return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read the full article...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');